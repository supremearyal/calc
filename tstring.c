#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tstring.h"

tstring* tstring_create() {
    tstring *t = malloc(sizeof(tstring));

    t->cap = TSTRING_DEFAULT_CAPACITY;
    t->str = malloc(sizeof(char) * (t->cap + 1));
    t->str[0] = '\0';
    t->len = 0;

    return t;
}

tstring* tstring_create_str(char *str) {
    tstring *t = tstring_create();

    int len = strlen(str);
    t->str = realloc(t->str, sizeof(char) * (strlen(str) + 1));
    strcpy(t->str, str);
    t->cap = t->len = len;

    return t;
}

tstring* tstring_copy(tstring *t) {
    tstring *c = malloc(sizeof(tstring));

    c->str = malloc(sizeof(char) * (t->cap + 1));
    strcpy(c->str, t->str);
    c->len = t->len;
    c->cap = t->cap;

    return c;
}

void tstring_grow(tstring *t) {
    t->cap *= 2;
    t->str = realloc(t->str, t->cap + 1);
}

void tstring_append_char(tstring *t, char c) {
    if(t->len == t->cap)
        tstring_grow(t);
    t->str[t->len++] = c;
    t->str[t->len] = '\0';
}

tstring* tstring_read_line() {
    tstring *t = tstring_create();

    int c;
    while((c = getchar()) != EOF && c != '\n') {
        if(t->len == t->cap)
            tstring_grow(t);
        t->str[t->len++] = c;
    }

    t->str[t->len] = '\0';
    return t;
}

void tstring_destroy(tstring *t) {
    free(t->str);
    free(t);
}
