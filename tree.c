#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "tree.h"
#include "variable.h"
#include "function.h"
#include "tstring.h"
#include "skip_list.h"

tree_node* tree_node_create(token *data, int len) {
    tree_node *t = malloc(sizeof(tree_node));

    t->data = data;
    t->len = len;
    t->nodes = malloc(sizeof(tree_node *) * len);

    return t;
}

void tree_node_print(tree_node *t, int level) {
    if(!t)
        return;

    for(int i = 0; i < level; ++i)
        fprintf(stderr, "  ");

    token_print(t->data);

    for(int i = 0; i < t->len; ++i)
        tree_node_print(t->nodes[i], level + 1);
}

double tree_node_evaluate(tree *t, tree_node *n,
                          skip_list *vars, skip_list *funcs) {
    if(t->error != NO_EVAL_ERROR)
        return 0;

    variable *v;
    function *s;
    tstring *c;
    skip_list_node *f;
    double r;

#define eval(x) tree_node_evaluate(t, x, vars, funcs)
    switch(n->data->type) {
        case NUMBER:
            return n->data->value_number;
        case VARIABLE:
            if(n->len > 0) {
                s = function_create(tstring_copy(n->data->value_variable), 0);
                f = skip_list_search(funcs, s);
                function_destroy(s);
                if(f)
                    return (((function *) f->data)->func)(eval(n->nodes[0]));
            } else {
                v = variable_create(tstring_copy(n->data->value_variable), 0);
                f = skip_list_search(vars, v);
                variable_destroy(v);
                if(f)
                    return ((variable *) f->data)->value;
                t->error = UNDEFINED_VARIABLE;
            }
            return -1;
        case OPERATOR_ADD:
            return eval(n->nodes[0]) + eval(n->nodes[1]);
        case OPERATOR_SUBTRACT:
            if(n->len > 1)
                return eval(n->nodes[0]) - eval(n->nodes[1]);
            else
                return -eval(n->nodes[0]);
        case OPERATOR_MULTIPLY:
            return eval(n->nodes[0]) * eval(n->nodes[1]);
        case OPERATOR_DIVIDE:
            return eval(n->nodes[0]) / eval(n->nodes[1]);
        case OPERATOR_EXPONENTIATE:
            return pow(eval(n->nodes[0]), eval(n->nodes[1]));
        case OPERATOR_EQUALS:
            r = eval(n->nodes[1]);
            c = tstring_copy(n->nodes[0]->data->value_variable);
            v = variable_create(c, r);
            f = skip_list_insert(vars, v);
            if(f) {
                ((variable *) f->data)->value = r;
                variable_destroy(v);
            }
            return r;
        default:
            return -1;
    }
#undef eval
}

void tree_node_destroy(tree_node *n) {
    if(!n)
        return;

    for(int i = 0; i < n->len; ++i)
        tree_node_destroy(n->nodes[i]);

    free(n->nodes);
    free(n);
}

tree* tree_create() {
    tree *t = malloc(sizeof(tree));

    t->head = 0;
    t->error = NO_EVAL_ERROR;

    return t;
}

void tree_print(tree *t) {
    fprintf(stderr, "=== SYNTAX TREE ===\n");
    tree_node_print(t->head, 0);
    fprintf(stderr, "===================\n");
}

double tree_evaluate(tree *t, skip_list *vars, skip_list *funcs) {
#ifdef DEBUG
    tree_print(t);
#endif

    return tree_node_evaluate(t, t->head, vars, funcs);
}

void tree_destroy(tree *t) {
    if(t->head)
        tree_node_destroy(t->head);

    free(t);
}
