#ifndef CALC_H
#define CALC_H

#define TSTRING_DEFAULT_CAPACITY 10

typedef struct {
    char *str;
    int cap;
    int len;
} tstring;

tstring* tstring_create();
tstring* tstring_create_str(char *str);
tstring* tstring_copy(tstring *t);
void tstring_grow(tstring *t);
void tstring_append_char(tstring *t, char c);
tstring* tstring_read_line();
void tstring_destroy(tstring *t);

#endif
