#include <stdlib.h>
#include <string.h>
#include "variable.h"

variable* variable_create(tstring *var, double value) {
    variable *v = malloc(sizeof(variable));

    v->var = var;
    v->value = value;

    return v;
}

int variable_compare(const void *a, const void *b) {
    const variable *va = (variable *) a;
    const variable *vb = (variable *) b;
    return strcmp(va->var->str, vb->var->str);
}

void variable_destroy(variable *v) {
    tstring_destroy(v->var);
    free(v);
}
