#ifndef VARIABLE_H
#define VARIABLE_H

#include "tstring.h"

typedef struct {
    tstring *var;
    double value;
} variable;

variable* variable_create(tstring *var, double value);
int variable_compare(const void *a, const void *b);
void variable_destroy(variable *v);

#endif
