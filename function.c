#include <stdlib.h>
#include <string.h>
#include "function.h"

function* function_create(tstring *name, double (*func)(double)) {
    function *f = malloc(sizeof(function));

    f->name = name;
    f->func = func;

    return f;
}

int function_compare(const void *a, const void *b) {
    const function *fa = (function *) a;
    const function *fb = (function *) b;
    return strcmp(fa->name->str, fb->name->str);
}

void function_destroy(function *f) {
    tstring_destroy(f->name);
    free(f);
}
