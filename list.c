#include <stdlib.h>
#include "list.h"

list* list_create() {
    return 0;
}

list* list_push(list *l, void *d) {
    list *n = malloc(sizeof(list));
    n->next = l;
    n->data = d;
    return n;
}

list* list_pop(list *l) {
    list *n = l->next;
    free(l);
    return n;
}

list* list_reverse(list *l) {
    list *a, *b, *p;
    p = 0;
    a = l;
    b = a ? a->next : 0;
    while(a && b) {
        a->next = p;
        p = a;
        a = b;
        b = b->next;
    }

    if(a)
        a->next = p;

    return a;
}

void list_destroy(list *l) {
    while(l) {
        list *i = l->next;
        free(l);
        l = i;
    }
}
