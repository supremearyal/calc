#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "skip_list.h"
#include "variable.h"
#include "function.h"

parser* parser_create(list *e) {
    parser *p = malloc(sizeof(parser));

    p->begin_token = e;
    p->current_token = e;

    p->error = NO_PARSE_ERROR;
    return p;
}

void parser_advance(parser *p) {
    p->current_token = p->current_token->next;
}

int parser_match(parser *p, token_type type) {
    token *t = (token *) p->current_token->data;
    return t->type == type;
}

int parser_done(parser *p) {
    return p->current_token == 0 || p->error != NO_PARSE_ERROR;
}

tree* parser_parse(parser *p) {
    tree_node *n = parser_statement(p);
    if(n && p->error == NO_PARSE_ERROR && parser_done(p)) {
        tree *t = tree_create();
        t->head = n;
        return t;
    } else {
        tree_node_destroy(n);
        p->error = SYNTAX_ERROR;
        return 0;
    }
}

tree_node* parser_statement(parser *p) {
    if(!parser_done(p) && (parser_match(p, NUMBER) ||
       parser_match(p, VARIABLE) || parser_match(p, OPERATOR_OPEN_PAREN) ||
       parser_match(p, OPERATOR_SUBTRACT))) {

        if(parser_match(p, VARIABLE) && p->current_token->next) {
            token *prev = (token *) p->current_token->data;
            token *t = (token *) p->current_token->next->data;

            if(t->type == OPERATOR_EQUALS) {
                parser_advance(p);
                parser_advance(p);

                tree_node *n = tree_node_create(t, 2);
                n->nodes[0] = tree_node_create(prev, 0);
                n->nodes[1] = parser_statement(p);

                return n;
            }
        }

        return parser_expression(p);
    } else {
        p->error = SYNTAX_ERROR;
        return 0;
    }
}

tree_node* parser_expression(parser *p) {
    if(!parser_done(p) && (parser_match(p, NUMBER) ||
       parser_match(p, VARIABLE) || parser_match(p, OPERATOR_OPEN_PAREN) ||
       parser_match(p, OPERATOR_SUBTRACT))) {
        tree_node *left = parser_term(p);

        while(!parser_done(p) && (parser_match(p, OPERATOR_ADD) ||
                                  parser_match(p, OPERATOR_SUBTRACT))) {
            token *prev = (token *) p->current_token->data;
            parser_advance(p);

            tree_node *n = tree_node_create(prev, 2);
            n->nodes[0] = left;
            n->nodes[1] = parser_term(p);
            left = n;
        }

        return left;
    } else {
        p->error = SYNTAX_ERROR;
        return 0;
    }
}

tree_node* parser_term(parser *p) {
    if(!parser_done(p) && (parser_match(p, NUMBER) ||
       parser_match(p, VARIABLE) || parser_match(p, OPERATOR_OPEN_PAREN) ||
       parser_match(p, OPERATOR_SUBTRACT))) {
        tree_node *left = parser_power(p);

        while(!parser_done(p) && (parser_match(p, OPERATOR_MULTIPLY) ||
                                  parser_match(p, OPERATOR_DIVIDE))) {
            token *prev = (token *) p->current_token->data;
            parser_advance(p);

            tree_node *n = tree_node_create(prev, 2);
            n->nodes[0] = left;
            n->nodes[1] = parser_power(p);
            left = n;
        }

        return left;
    } else {
        p->error = SYNTAX_ERROR;
        return 0;
    }
}

tree_node* parser_power(parser *p) {
    int neg = 0;

    if(parser_done(p))
        goto err;

    token *sub = 0;
    if(parser_match(p, OPERATOR_SUBTRACT)) {
        sub = (token *) p->current_token->data;
        neg = 1;
        parser_advance(p);
    }

    if(parser_done(p))
        goto err;

    if(parser_match(p, NUMBER) || parser_match(p, VARIABLE) ||
       parser_match(p, OPERATOR_SUBTRACT) ||
       parser_match(p, OPERATOR_OPEN_PAREN)) {
        tree_node *left = parser_factor(p);

        if(!parser_done(p) && parser_match(p, OPERATOR_EXPONENTIATE)) {
            token *prev = (token *) p->current_token->data;
            parser_advance(p);

            tree_node *n = tree_node_create(prev, 2);
            n->nodes[0] = left;
            n->nodes[1] = parser_power(p);
            left = n;
        }

        if(neg) {
            tree_node *n = tree_node_create(sub, 1);
            n->nodes[0] = left;
            left = n;
        }

        return left;
    }

err:
    p->error = SYNTAX_ERROR;
    return 0;
}

tree_node* parser_factor(parser *p) {
    if(parser_done(p))
        goto err;

    if(parser_match(p, NUMBER) || parser_match(p, VARIABLE)) {
        token *prev = (token *) p->current_token->data;

        if(prev->type == VARIABLE && p->current_token->next) {
            token *cur = (token *) p->current_token->next->data;
            if(cur->type == OPERATOR_OPEN_PAREN) {
                parser_advance(p);
                parser_advance(p);
                tree_node *n = tree_node_create(prev, 1);
                n->nodes[0] = parser_expression(p);
                if(!parser_done(p) && parser_match(p, OPERATOR_CLOSE_PAREN))
                    parser_advance(p);
                return n;
            }
        }

        parser_advance(p);
        return tree_node_create(prev, 0);
    } else if(parser_match(p, OPERATOR_OPEN_PAREN)) {
        parser_advance(p);
        tree_node *n = parser_expression(p);
        if(!parser_done(p) && parser_match(p, OPERATOR_CLOSE_PAREN))
            parser_advance(p);
        return n;
    }

err:
    p->error = SYNTAX_ERROR;
    return 0;
}

void parser_reset(parser *p, list *e) {
    list_destroy(p->begin_token);
    p->begin_token = e;
    p->current_token = e;
    p->error = NO_PARSE_ERROR;
}

void parser_destroy(parser *p) {
    if(p->begin_token)
        list_destroy(p->begin_token);

    free(p);
}

