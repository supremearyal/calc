#ifndef TOKENIZER_H
#define TOKENIZER_H

#include "tstring.h"
#include "list.h"

typedef enum {NUMBER, VARIABLE, OPERATOR_ADD,
              OPERATOR_SUBTRACT, OPERATOR_MULTIPLY,
              OPERATOR_DIVIDE, OPERATOR_EXPONENTIATE,
              OPERATOR_OPEN_PAREN, OPERATOR_CLOSE_PAREN,
              OPERATOR_EQUALS, INVALID_TOKEN} token_type;

typedef struct {
    token_type type;
    union {
        char value_operator;
        double value_number;
        tstring *value_variable;
    };
} token;

token* token_create();
token_type char_to_token(char c);
char token_to_char(token_type t);
list* tokenize(char *s);
void token_print(token *t);
void tokens_print(list *l);
void token_destroy(token *t);

#endif
