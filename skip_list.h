#ifndef SKIP_LIST_H
#define SKIP_LIST_H

#define MAX_LEVELS 32

typedef struct skip_list_node {
    void *data;
    struct skip_list_node **next;
    int levels;
} skip_list_node;

typedef struct {
    skip_list_node *head;
    int (*cmp)(const void *, const void *);
} skip_list;

skip_list* skip_list_create(int (*cmp)(const void *, const void *));
skip_list_node* skip_list_insert(skip_list *s, void *data);
skip_list_node* skip_list_search(skip_list *s, void *data);
void skip_list_delete(skip_list *s, void *data);
void skip_list_destroy(skip_list *s);

#endif
