#ifndef PARSER_H
#define PARSER_H

#include "list.h"
#include "tokenizer.h"
#include "skip_list.h"
#include "tree.h"

typedef enum {NO_PARSE_ERROR, SYNTAX_ERROR} parse_error;

typedef struct {
    list *begin_token;
    list *current_token;
    parse_error error;
} parser;

parser* parser_create(list *e);
void parser_advance(parser *p);
int parser_match(parser *p, token_type type);
int parser_done(parser *p);
tree* parser_parse(parser *p);
tree_node* parser_statement(parser *p);
tree_node* parser_expression(parser *p);
tree_node* parser_term(parser *p);
tree_node* parser_power(parser *p);
tree_node* parser_factor(parser *p);
void parser_reset(parser *p, list *e);
void parser_destroy(parser *p);

#endif
