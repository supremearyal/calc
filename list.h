#ifndef LIST_H
#define LIST_H

typedef struct list {
    void *data;
    struct list *next;
} list;

list* list_create();
list* list_push(list *l, void *d);
list* list_pop(list *l);
list* list_reverse(list *l);
void list_destroy(list *l);

#endif
