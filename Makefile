CC = gcc
CFLAGS = -Wall -Werror -Wextra -std=c99
LFLAGS = -lm

TARGET = calc
SOURCE = $(wildcard *.c)
OBJECT = $(SOURCE:.c=.o)

debug: CFLAGS += -g -DDEBUG
debug: $(TARGET)

release: CFLAGS += -O2
release: $(TARGET)

$(TARGET): $(OBJECT)
	$(CC) $(OBJECT) -o $(TARGET) $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f $(TARGET) $(TARGET).exe $(OBJECT)
