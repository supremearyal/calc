#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "tokenizer.h"

token* token_create(token_type type) {
    token *t = malloc(sizeof(token));
    t->type = type;

    switch(type) {
        case NUMBER:
            t->value_number = 0;
            break;
        case VARIABLE:
            t->value_variable = 0;
            break;
        case OPERATOR_ADD:
        case OPERATOR_SUBTRACT:
        case OPERATOR_MULTIPLY:
        case OPERATOR_DIVIDE:
        case OPERATOR_EXPONENTIATE:
        case OPERATOR_OPEN_PAREN:
        case OPERATOR_CLOSE_PAREN:
        case OPERATOR_EQUALS:
        case INVALID_TOKEN:
            t->value_operator = token_to_char(type);
            break;
    }

    return t;
}

token_type char_to_token(char c) {
    switch(c) {
        case '+': return OPERATOR_ADD;
        case '-': return OPERATOR_SUBTRACT;
        case '*': return OPERATOR_MULTIPLY;
        case '/': return OPERATOR_DIVIDE;
        case '^': return OPERATOR_EXPONENTIATE;
        case '(': return OPERATOR_OPEN_PAREN;
        case ')': return OPERATOR_CLOSE_PAREN;
        case '=': return OPERATOR_EQUALS;
        default: return INVALID_TOKEN;
    }
}

char token_to_char(token_type t) {
    switch(t) {
        case OPERATOR_ADD: return '+';
        case OPERATOR_SUBTRACT: return '-';
        case OPERATOR_MULTIPLY: return '*';
        case OPERATOR_DIVIDE: return '/';
        case OPERATOR_EXPONENTIATE: return '^';
        case OPERATOR_OPEN_PAREN: return '(';
        case OPERATOR_CLOSE_PAREN: return ')';
        case OPERATOR_EQUALS: return '=';
        default: return '~';
    }
}

list* tokenize(char *s) {
    list *l = list_create();
    list *i;
    int line = 1;
    char *begin = s;
    char *err_msg;

    while(*s) {
        if(isdigit(*s) || *s == '.') {
            token *t = token_create(NUMBER);
            char *end;
            t->value_number = strtod(s, &end);
            if(s == end) {
                token_destroy(t);
                err_msg = "Couldn't parse number.";
                goto err;
            }
            s = end;
            l = list_push(l, t);
        } else if(isalpha(*s)) {
            token *t = token_create(VARIABLE);
            t->value_variable = tstring_create();
            while(*s && isalnum(*s)) {
                tstring_append_char(t->value_variable, *s);
                s++;
            }
            l = list_push(l, t);
        } else if(*s == '+' || *s == '-' || *s == '*' ||
                  *s == '/' || *s == '^' || *s == '(' ||
                  *s == ')' || *s == '=') {
            token *t = token_create(char_to_token(*s));
            t->value_operator = *s;
            l = list_push(l, t);
            s++;
        } else if(*s == '\n') {
            line++;
        } else if(isblank(*s)) {
            s++;
        } else {
            err_msg = "Unrecognized character.";
            goto err;
        }
    }

    return list_reverse(l);

err:
    i = l;
    while(i) {
        token *t = (token *)(i->data);
        token_destroy(t);
        i = i->next;
    }
    list_destroy(l);
    printf("Error on line %d, column %d: %s\n",
           line, (int)(s - begin) + 1, err_msg);
    return 0;
}

void token_print(token *t) {
    switch(t->type) {
        case NUMBER:
            fprintf(stderr, "NUM: %f\n", t->value_number);
            break;
        case VARIABLE:
            fprintf(stderr, "VAR: %s\n", t->value_variable->str);
            break;
        case OPERATOR_ADD:
        case OPERATOR_SUBTRACT:
        case OPERATOR_MULTIPLY:
        case OPERATOR_DIVIDE:
        case OPERATOR_EXPONENTIATE:
        case OPERATOR_OPEN_PAREN:
        case OPERATOR_CLOSE_PAREN:
        case OPERATOR_EQUALS:
        case INVALID_TOKEN:
            fprintf(stderr, "OPR: %c\n", t->value_operator);
            break;
    }
}

void tokens_print(list *l) {
    while(l) {
        token *t = (token *) l->data;
        token_print(t);
        l = l->next;
    }
}

void token_destroy(token *t) {
    if(t->type == VARIABLE)
        tstring_destroy(t->value_variable);
    free(t);
}
