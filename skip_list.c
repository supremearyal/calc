/* Code derived from:
 * - http://en.literateprograms.org/Skip_list_(C)
 * - http://igoro.com/archive/skip-lists-are-fascinating/
 */

#include <stdlib.h>
#include <string.h>
#include "skip_list.h"

static skip_list_node* skip_list_node_create(void *data) {
    skip_list_node *n = malloc(sizeof(skip_list_node));
    n->data = data;
    n->levels = 1;
    n->next = calloc(1, sizeof(skip_list_node *));

    return n;
}

static void skip_list_node_destroy(skip_list_node *n) {
    free(n->next);
    free(n);
}

static int get_levels() {
    int i = 1;

    while(i < MAX_LEVELS && rand() % 2)
        i++;

    return i;
}

skip_list* skip_list_create(int (*cmp)(const void *, const void *)) {
    skip_list *s = malloc(sizeof(skip_list));
    s->head = skip_list_node_create(0);
    s->cmp = cmp;

    return s;
}

skip_list_node* skip_list_insert(skip_list *s, void *data) {
    skip_list_node *f = skip_list_search(s, data);
    if(f)
        return f;

    skip_list_node* h = s->head;
    skip_list_node *n = skip_list_node_create(data);
    n->levels = get_levels();
    n->next = realloc(n->next, n->levels * sizeof(skip_list_node *));
    memset(n->next, 0, n->levels * sizeof(skip_list_node *));

    for(int level = h->levels - 1; level >= 0; --level) {
        while(h->next[level] && s->cmp(h->next[level]->data, data) < 0)
            h = h->next[level];

        if(level < n->levels) {
            n->next[level] = h->next[level];
            h->next[level] = n;
        }
    }

    if(n->levels > s->head->levels)
        s->head->next = realloc(s->head->next,
                                n->levels * sizeof(skip_list_node *));
    while(s->head->levels < n->levels)
        s->head->next[s->head->levels++] = n;

    return 0;
}

skip_list_node* skip_list_search(skip_list *s, void *data) {
    skip_list_node* h = s->head;
    for(int level = h->levels - 1; level >= 0; --level)
        while(h->next[level] && s->cmp(h->next[level]->data, data) < 0)
            h = h->next[level];

    h = h->next[0];
    if(h && !s->cmp(h->data, data))
        return h;
    else
        return 0;
}

void skip_list_delete(skip_list *s, void *data) {
    skip_list_node *f = skip_list_search(s, data);
    if(!f)
        return;

    skip_list_node* h = s->head;
    for(int level = h->levels - 1; level >= 0; --level) {
        while(h->next[level] && s->cmp(h->next[level]->data, data) < 0)
            h = h->next[level];

        if(h->next[level] == f)
            h->next[level] = h->next[level]->next[level];
    }

    skip_list_node_destroy(f);
}

void skip_list_destroy(skip_list *s) {
    skip_list_node *i = s->head;
    while(s->head) {
        i = s->head->next[0];
        skip_list_node_destroy(s->head);
        s->head = i;
    }
    free(s);
}
