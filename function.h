#ifndef FUNCTION_H
#define FUNCTION_H

#include "tstring.h"

typedef struct {
    tstring *name;
    double (*func)(double);
} function;

function* function_create();
int function_compare(const void *a, const void *b);
void function_destroy(function *f);

#endif
