#ifndef TREE_H
#define TREE_H

#include "tokenizer.h"
#include "skip_list.h"

typedef token_type tree_type;

typedef enum {NO_EVAL_ERROR, DIV_BY_ZERO, UNDEFINED_VARIABLE,
              UNDEFINED_FUNCTION} eval_error;

typedef struct tree_node {
    token *data;
    int len;
    struct tree_node **nodes;
} tree_node;

typedef struct {
    tree_node *head;
    eval_error error;
} tree;

tree_node* tree_node_create(token *data, int len);
void tree_node_print(tree_node *t, int level);
void tree_node_destroy(tree_node *n);

tree* tree_create();
void tree_print(tree *t);
double tree_evaluate(tree *t, skip_list *vars, skip_list *funcs);
void tree_destroy(tree *t);

#endif
