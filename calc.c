#include <stdio.h>
#include <math.h>
#include <string.h>
#include "list.h"
#include "skip_list.h"
#include "tstring.h"
#include "tokenizer.h"
#include "variable.h"
#include "function.h"
#include "parser.h"

#define ADD_VAR(vars, s, v) skip_list_insert(vars,\
        variable_create(tstring_create_str(s), v))

#define ADD_FUNC(funcs, s, f) skip_list_insert(funcs,\
        function_create(tstring_create_str(s), f))

void init_vars(skip_list *vars) {
    ADD_VAR(vars, "pi", atan(1) * 4);
    ADD_VAR(vars, "e", exp(1));
}

void init_funcs(skip_list *funcs) {
    ADD_FUNC(funcs, "sin", sin);
    ADD_FUNC(funcs, "cos", cos);
    ADD_FUNC(funcs, "tan", tan);
    ADD_FUNC(funcs, "asin", asin);
    ADD_FUNC(funcs, "acos", acos);
    ADD_FUNC(funcs, "atan", atan);
    ADD_FUNC(funcs, "cosh", cosh);
    ADD_FUNC(funcs, "sinh", sinh);
    ADD_FUNC(funcs, "tanh", tanh);
    ADD_FUNC(funcs, "exp", exp);
    ADD_FUNC(funcs, "log", log);
    ADD_FUNC(funcs, "log10", log10);
    ADD_FUNC(funcs, "sqrt", sqrt);
    ADD_FUNC(funcs, "ceil", ceil);
    ADD_FUNC(funcs, "abs", fabs);
    ADD_FUNC(funcs, "floor", floor);
    ADD_FUNC(funcs, "mod", fmod);
}

int main() {
    skip_list *vars = skip_list_create(variable_compare);
    skip_list *funcs = skip_list_create(function_compare);
    variable *v = variable_create(tstring_create_str("ans"), 0);
    parser *p = parser_create(0);

    init_vars(vars);
    init_funcs(funcs);

    while(1) {
        tstring *t = tstring_read_line();
        if(!strcmp(t->str, "quit")) {
            tstring_destroy(t);
            break;
        }
        list *l = tokenize(t->str);
        if(l) {
            parser_reset(p, l);
            tree *ast = parser_parse(p);

            if(ast) {
                double r = tree_evaluate(ast, vars, funcs);

                if(ast->error == NO_EVAL_ERROR) {
                    v->value = r;
                    skip_list_node *f = skip_list_insert(vars, v);
                    if(f) {
                        variable *fv = (variable *) f->data;
                        fv->value = r;
                    }
                    printf("%f\n", r);
                } else if(ast->error == UNDEFINED_VARIABLE) {
                    printf("Undefined variable!\n");
                } else if(ast->error == UNDEFINED_FUNCTION) {
                    printf("Undefined function!\n");
                }

                tree_destroy(ast);
            } else {
                printf("Syntax error!\n");
            }

            list *i = l;
            while(i) {
                token *tk = (token *) i->data;
                token_destroy(tk);
                i = i->next;
            }
        }

        tstring_destroy(t);
    }

    if(!skip_list_search(vars, v))
        variable_destroy(v);

    for(skip_list_node *i = *vars->head->next; i; i = *i->next)
        variable_destroy(i->data);
    skip_list_destroy(vars);

    for(skip_list_node *i = *funcs->head->next; i; i = *i->next)
        function_destroy(i->data);
    skip_list_destroy(funcs);

    parser_destroy(p);

    return 0;
}
